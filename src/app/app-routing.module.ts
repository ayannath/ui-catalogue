import { OutlineButtonComponent } from './outline-button/outline-button.component';
import { FloatButtonComponent } from './float-button/float-button.component';
import { FlatButtonComponent } from './flat-button/flat-button.component';
import { IconButtonComponent } from './icon-button/icon-button.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { AppBarComponent } from './app-bar/app-bar.component';
import { GridViewComponent } from './grid-view/grid-view.component';
import { WidgetDetailsComponent } from './widget-details/widget-details.component';
import { DefaultComponent } from './default/default.component';
import { NgModule, } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [

  { path: '', component: DefaultComponent },
  { path: 'widget', component: WidgetDetailsComponent },
  { path: 'grid', component: GridViewComponent },
  { path: 'appbar', component: AppBarComponent },
  { path: 'dropdown', component: DropdownComponent },
  { path: 'iconbtn', component: IconButtonComponent },
  { path: 'fltbtn', component: FlatButtonComponent },
  { path: 'floatbtn', component: FloatButtonComponent },
  { path: 'outlinebtn', component: OutlineButtonComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
