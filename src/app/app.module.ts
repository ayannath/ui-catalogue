import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WidgetDetailsComponent } from './widget-details/widget-details.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { GridViewComponent } from './grid-view/grid-view.component';
import { AppBarComponent } from './app-bar/app-bar.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { IconButtonComponent } from './icon-button/icon-button.component';
import { FlatButtonComponent } from './flat-button/flat-button.component';
import { FloatButtonComponent } from './float-button/float-button.component';
import { OutlineButtonComponent } from './outline-button/outline-button.component';

@NgModule({
  declarations: [
    AppComponent,
    WidgetDetailsComponent,
    HeaderComponent,
    FooterComponent,
    GridViewComponent,
    AppBarComponent,
    DropdownComponent,
    IconButtonComponent,
    FlatButtonComponent,
    FloatButtonComponent,
    OutlineButtonComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule,
    CarouselModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
